﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlusController : ControllerBase
    {
        

        [HttpPost]
        public int Get([FromBody] int[] a)
        {
            return Math.Abs(a.Where(i=> (i & 1) == 1).Where((x,j)=>(j & 1) == 1).Sum());
        }
    }
}
