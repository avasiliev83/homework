using Microsoft.AspNetCore.Mvc.Testing;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTestProject1
{
    public class Tests
    {
        public WebApplicationFactory<WebApplication3.Startup> factory { get; set; }

        [SetUp]
        public void Setup()
        {
            factory = new WebApplicationFactory<WebApplication3.Startup>();

        }
        
        [TestCase("\"abcxcba\"", "true")]
        [TestCase("\"abccba\"", "true")]
        [TestCase("\"TestToPalindrom\"", "false")]
        public async Task Test1(string _case, string _response)
        {
            // Arrange
            var client = factory.CreateClient();

            // Act
            var response = await client.PostAsync("/palindrom", new StringContent(_case, Encoding.UTF8, "application/json"));

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            Assert.AreEqual("application/json; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
            var content = await response.Content.ReadAsStringAsync();

            Assert.AreEqual(_response, content);

        }

        [TestCase("abc", HttpStatusCode.BadRequest)]
        public async Task TestBadRequest(string _case, HttpStatusCode _responseStatusCode)
        {
            // Arrange
            var client = factory.CreateClient();

            // Act
            var response = await client.PostAsync("/palindrom", new StringContent(_case, Encoding.UTF8, "application/json"));

            // Assert

            Assert.AreEqual(_responseStatusCode,
                response.StatusCode);

        }
    }
}