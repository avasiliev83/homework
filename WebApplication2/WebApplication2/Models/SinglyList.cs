﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class SinglyList<T>:IEnumerable<T>
    {
        // Первый элемент списка
        private Item<T> head = null;
        public Item<T> Head { get => head; }
        
        // Последний элемент списка
        private Item<T> tail = null;
        public Item<T> Tail { get => tail; } 

        /// Количество элементов списка
        private int count = 0;

        public int Count
        {
            get => count;
        }
                
        // Добавить данные в связный список
        public void Add(T data)
        {
            // Проверяем входные аргументы на null
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            // Создаем новый элемент связного списка
            var item = new Item<T>(data);
            
            // Если связный список пуст, то добавляем созданный элемент в начало,
            // иначе добавляем этот элемент как следующий за крайним элементом
            if (head == null)
            {
                head = item;
            }
            else
            {
                tail.Next = item;
            }

            // Устанавливаем этот элемент последним
            tail = item;

            // Увеличиваем счетчик количества элементов
            count++;
        }
        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }


    }



}
