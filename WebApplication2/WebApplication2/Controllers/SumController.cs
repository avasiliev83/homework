﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SumController : ControllerBase
    {
        public class Data
        {

            [JsonPropertyName("number1")]
            [JsonConverter(typeof(ArrayToSinglyListConverter))]
            public object number1 { get; set; }
            
            [JsonPropertyName("number2")]
            [JsonConverter(typeof(ArrayToSinglyListConverter))]
            public object number2 { get; set; }
        }

        
        [HttpPost]
        public SinglyList<int> Get([FromBody] Data data)
        {
            return SinglyListOperations.Sum((SinglyList<int>)data.number1, (SinglyList<int>)data.number2);
        }
    }
}
