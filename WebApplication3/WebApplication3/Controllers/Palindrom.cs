﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApplication3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PalindromController : ControllerBase
    {

        [HttpPost]
        public bool Post([FromBody]string data)
        {
            if (String.IsNullOrEmpty(data))
                return false;
            else
            {
                return data.Equals(new string(data.Reverse().ToArray()));
            }
        }
    }
}
