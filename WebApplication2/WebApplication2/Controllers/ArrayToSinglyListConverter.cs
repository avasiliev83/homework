﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    internal class ArrayToSinglyListConverter : JsonConverter<object>
    {
        public override object Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var results = new SinglyList<int>();
            reader.Read();
            while (reader.TokenType != JsonTokenType.EndArray)
            {
                var result = reader.TokenType == JsonTokenType.Number
                    ? reader.GetInt16()
                    : Skip(ref reader);
                results.Add(result);
                reader.Read();
            }
            return results;
        }
        private int Skip(ref Utf8JsonReader reader)
        {
            if (reader.TokenType == JsonTokenType.PropertyName)
            {
                reader.Read();
            }

            if (reader.TokenType == JsonTokenType.StartObject
                || reader.TokenType == JsonTokenType.StartArray)
            {
                var depth = reader.CurrentDepth;
                while (reader.Read() && depth <= reader.CurrentDepth) { }
            }
            return 0;
        }
        public override void Write(Utf8JsonWriter writer, object value, JsonSerializerOptions options)
        {
            throw new NotImplementedException("Exception on write");
        }
    }
}