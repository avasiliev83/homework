﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class SinglyListOperations
    {
        //функция поразрядного суммирования списков
        public static SinglyList<int> Sum(SinglyList<int> l1, SinglyList<int> l2)
        {
            //выбираем список наибольшей длины, в него будем писать результат
            var result = l1.Count >= l2.Count ? l1 : l2;
            //в second заносим список покороче
            var second = l1.Count < l2.Count ? l1 : l2;
            //устанавливаем указатели на начало списков
            var itemres = result.Head;
            var itemsec = second.Head;
            //инициализируем переносимый разряд при сложении в столбик
            int div = 0;
            //сначала складываем все разряды
            for (int i = 0; i < second.Count; i++)
            {
                int res = itemres.Data + itemsec.Data + div;
                itemres.Data = res % 10;
                div = res / 10;
                itemres = itemres.Next;
                itemsec = itemsec.Next;
            }
            //проверяем разряд, и есть ли item'ы, куда его записать
            while (div > 0 && itemres != null)
            {
                int res = itemres.Data + div;
                itemres.Data = res % 10;
                div = res / 10;
                itemres = itemres.Next;
            }
            //если на конце были девятки, то надо добавить новый элемент в результат
            if (div > 0)
            {
                result.Add(div);
            }
            return result;
        }
    }
}
